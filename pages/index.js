import { Inter } from "next/font/google";
import { collection, addDoc } from "firebase/firestore";

import Link from "next/link";

const inter = Inter({ subsets: ["latin"] });

const featuresData = [
  {
    id: 1,
    heading: "Note Creation",
    text: "The ability to create new notes with a clean and simple interface. Users can start typing or writing their notes as soon as they open the app.",
  },
  {
    id: 2,
    heading: "Note Organization",
    text: "Allowing users to categorize and group their notes using folders",
  },
  {
    id: 3,
    heading: "Search Functionality",
    text: "A powerful search feature that helps users quickly find specific notes based on keywords or phrases within the notes.",
  },
  {
    id: 4,
    heading: "Multiple User Creation",
    text: " User Can Create Multiple account.",
  },
  {
    id: 5,
    heading: "Syncing and Cloud Storage",
    text: "The option to sync notes across multiple devices, such as smartphones, tablets, and computers, and store them securely in the cloud.",
  },
];

export default function Home() {
  return (
    <>
      <div
        className="mid-div flex flex-col justify-center items-center"
        style={{ height: "80%" }}
      >
        <div className=" border-dashed border-spacing-3 flex flex-col items-center rounded-xl border-2 border-gray-500 w-2/4 h-4/5">
          <h1 className=" text-2xl mt-2 mb-10">Features</h1>
          {featuresData.map((textData) => {
            return (
              <div className="flex h-10 w-10/12 mb-6 items-center">
                <div className="border-2 px-2 rounded-full pt-1 mr-4">
                  {textData.id}
                </div>
                <p className="">
                  <span className=" underline">{textData.heading}: </span>
                  {textData.text}
                </p>
              </div>
            );
          })}
        </div>
        <div className="mt-5">
          <button className=" px-6 py-3 text-lg bg-gray-900 mr-10 rounded-lg shadow-inner shadow-slate-700 text-slate-600 hover:text-slate-300">
            <Link href="/login">LogIn</Link>
          </button>
          <button className="px-4 py-3 text-lg bg-gray-900 ml-10 rounded-lg shadow-inner shadow-slate-700 text-slate-600 hover:text-slate-300">
            <Link href="/signup">SignUp</Link>
          </button>
        </div>
      </div>
    </>
  );
}
