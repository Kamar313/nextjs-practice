import React from "react";
import "@/styles/globals.css";
import Link from "next/link";
import Image from "next/image";


export default function App({ Component, pageProps }) {
  return (
    <div className=" mx-16" style={{ height: "80vh" }}>
      <div className="heading flex items-center">
        <Link href="/">
          <Image src="/diary.png" width={100} height={100} alt="logo" />
        </Link>
        <h1 className=" ml-4 text-2xl font-bold text-slate-600">
          Notes WebApp
        </h1>
      </div>

      <Component {...pageProps} />

      <div className=" flex justify-center items-end h-1/5 text-slate-600">
        <p>Created By Mohmad </p>
      </div>
    </div>
  );
}
