import Link from "next/link";

export default function Login() {
  return (
    <>
      <div
        className="mid-div flex flex-col justify-center items-center"
        style={{ height: "80%" }}
      >
        <div className=" border-dashed border-spacing-3 flex flex-col items-center justify-center rounded-xl border-2 border-gray-500 w-2/4 h-4/5">
          <div className=" w-1/2 mb-5 rounded-lg h-10 p-2 bg-slate-100">
            <input
              type="email"
              placeholder="Enter Your Email"
              className="w-full text-slate-600 placeholder:text-center outline-none bg-slate-100"
            />
          </div>
          <div className="p-2 mt-5 rounded-lg w-1/2 h-10 bg-slate-100">
            <input
              type="password"
              placeholder="Enter Your Password"
              className="w-full text-slate-600 placeholder:text-center outline-none bg-slate-100"
            />
          </div>
          <button className="p-2 mt-14 rounded-lg w-1/2 h-10 bg-slate-100 text-slate-600 shadow-inner shadow-slate-400">
            Log In
          </button>
          <div className="mt-7">
            <p className="text-slate-600">
              Don't have an account?{" "}
              <span className=" underline cursor-pointer text-white">
                <Link href="/signup">Sign up</Link>
              </span>
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
