import React, { useState } from "react";
import Link from "next/link";
import { auth } from "@/component/firebase";
import { createUserWithEmailAndPassword } from "firebase/auth";

export default function Signup() {
  const [usereName, setUserName] = useState("");
  const [userEmail, setUserEmail] = useState("");
  const [userPassword, setUserPassword] = useState("");
  function signUpHandler() {
    createUserWithEmailAndPassword(auth, userEmail, userPassword)
      .then((user) => {
        console.log(user);
      })
      .catch((err) => console.log(err));
  }
  return (
    <>
      <div
        className="mid-div flex flex-col justify-center items-center"
        style={{ height: "80%" }}
      >
        <div className=" border-dashed border-spacing-3 flex flex-col items-center justify-center rounded-xl border-2 border-gray-500 w-2/4 h-4/5">
          <div className=" w-1/2 mb-5 rounded-lg h-10 p-2 bg-slate-100">
            <input
              type="email"
              placeholder="Enter Your Email"
              className="w-full text-slate-600 placeholder:text-center outline-none bg-slate-100"
              value={userEmail}
              onChange={(e) => setUserEmail(e.target.value)}
            />
          </div>
          <div className="p-2 mt-5 mb-5 rounded-lg w-1/2 h-10 bg-slate-100">
            <input
              type="number"
              placeholder="Enter Your Number"
              className="w-full text-slate-600 bg-slate-100 placeholder:text-center outline-none [appearance:textfield] [&::-webkit-outer-spin-button]:appearance-none [&::-webkit-inner-spin-button]:appearance-none "
            />
          </div>
          <div className="p-2 mt-5 rounded-lg w-1/2 h-10 bg-slate-100">
            <input
              type="password"
              placeholder="Enter Your Password"
              className="w-full text-slate-600 bg-slate-100 placeholder:text-center outline-none "
              value={userPassword}
              onChange={(e) => setUserPassword(e.target.value)}
            />
          </div>
          <button
            onClick={() => signUpHandler()}
            className="p-2 mt-14 rounded-lg w-1/2 h-10 bg-slate-100 text-slate-600 shadow-inner shadow-slate-400"
          >
            Sign Up
          </button>
          <div className="mt-7">
            <p className="text-slate-600">
              Already have an account?{" "}
              <span className=" underline cursor-pointer text-white">
                <Link href="/login">Log In</Link>
              </span>
            </p>
          </div>
        </div>
      </div>
    </>
  );
}
