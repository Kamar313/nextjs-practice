// Import the functions you need from the SDKs you need

import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import { getFirestore } from "firebase/firestore";
import { getAuth } from "firebase/auth";
// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyD3h9e6iQ5HJODHBwHuGZ_E_phxG9LOn7g",
  authDomain: "notes-29173.firebaseapp.com",
  projectId: "notes-29173",
  storageBucket: "notes-29173.appspot.com",
  messagingSenderId: "355095751420",
  appId: "1:355095751420:web:e6c8eed9350faef3db4579",
  measurementId: "G-SEM7HJYBMG",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
const db = getFirestore(app);
export const auth = getAuth();
export default db;
